from flask import render_template
from app import app, db

@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(error):
    # To prevent failed db sessions from interfering with any db accesses triggered 
    # by the template, the session is rolled back to a clean slate
    db.session.rollback() 
    return render_template('500.html'), 500